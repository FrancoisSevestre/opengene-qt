#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    // Set main window
    resize(1000,500);
    setWindowTitle(tr("OpenGene"));
    setWindowIcon(QIcon("://icons/opengeneIcon.png"));
    QWidget *centralZone = new QWidget; // Setup central zone
    setCentralWidget(centralZone);

    QVBoxLayout *centralLayout = new QVBoxLayout(centralZone); // Layout: separate the central zone vertically
    centralZone->setLayout(centralLayout);

    QHBoxLayout *upperLayout = new QHBoxLayout();
    centralLayout->addLayout(upperLayout);

    QPushButton *test1 = new QPushButton("test1");
    centralLayout->addWidget(test1);
    QPushButton *test2 = new QPushButton("test2");
    upperLayout->addWidget(test2);
    QPushButton *test3 = new QPushButton("test3");
    upperLayout->addWidget(test3);

    createMenus();
    createToolBars();
    statusBar()->showMessage(tr("Prêt!"));
}

void MainWindow::createMenus()
{

// Menu and tool Bars
QMenuBar *mainWindowMenuBar = new QMenuBar(this);
QToolBar *mainToolBar = new QToolBar(tr("Barre d'outils"), this);

// Menus
QMenu *fileMenu = new QMenu(tr("&Fichier"), this);
QMenu *editionMenu = new QMenu(tr("&Edition"), this);
QMenu *viewMenu = new QMenu(tr("&View"), this);
QMenu *helpMenu = new QMenu(tr("&Help"), this);

//Menu actions
QAction *quit = new QAction(this);
    quit->setText(tr("&Quitter"));
    quit->setIcon(QIcon("://icons/quit.png"));
    quit->setShortcut(QKeySequence(Qt::CTRL | Qt::Key_Q));

QAction *newSequence = new QAction(this);
    newSequence->setText(tr("&Nouvelle séquence"));
    newSequence->setIcon(QIcon("://icons/newSequence.png"));
    newSequence->setShortcut(QKeySequence(Qt::CTRL | Qt::Key_N));

QAction *openFile = new QAction(this);
    openFile->setText(tr("&Ouvrir"));
    openFile->setIcon(QIcon("://icons/openFile.png"));
    openFile->setShortcut(QKeySequence(Qt::CTRL | Qt::Key_O));

QAction *showToolBar = new QAction(this);
    showToolBar->setText(tr("Afficher la barre d'outils"));
    showToolBar->setCheckable(true);
    showToolBar->setChecked(true);


    // Assemble
setMenuWidget(mainWindowMenuBar); // Set menu bar
addToolBar(mainToolBar); // Set tool bar

// Add menus to menu bar
mainWindowMenuBar->addMenu(fileMenu);
mainWindowMenuBar->addMenu(editionMenu);
mainWindowMenuBar->addMenu(viewMenu);
mainWindowMenuBar->addMenu(helpMenu);

// Add actions to the menus

fileMenu->addAction(newSequence);
fileMenu->addAction(openFile);
fileMenu->addAction(quit);

editionMenu->addAction(showToolBar);

// Add actions to toolbar
mainToolBar->addAction(quit);
mainToolBar->addSeparator();
mainToolBar->addAction(newSequence);
mainToolBar->addAction(openFile);

    // Connect actions to slots
connect(quit, &QAction::triggered, this, &MainWindow::close);
connect(showToolBar, &QAction::triggered, mainToolBar, &QToolBar::setVisible);

}

void MainWindow::createToolBars()
{



}

MainWindow::~MainWindow()
{
}

